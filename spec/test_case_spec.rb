require_relative '../lib/table'
require_relative '../lib/robot'

describe Robot do

  before(:each) do
    @table = Table.new 5, 5
    @robot = Robot.new @table
  end

  it 'raise exceptions' do
    expect { @robot.place(1, 'Delhi', nil) }.to raise_error(TypeError)
    expect { @robot.place(1, 0, :wrongface) }.to raise_error(TypeError)
    expect { @robot.place(nil, nil, :north) }.to raise_error(TypeError)
    expect { @robot.command("PLACE12NORTH") }.to raise_error(ArgumentError)
    expect { @robot.command("LEFFT") }.to raise_error(ArgumentError)
    expect { @robot.command("RIGHTT") }.to raise_error(ArgumentError)
  end

  it 'placed in table' do
    expect(@robot.place(7, 6, :west)).to eq(false)
    expect(@robot.place(-3, 2, :east)).to eq(false)
    expect(@robot.place(1, 2, :north)).to eq(true)
    expect(@robot.place(3, 3, :south)).to eq(true)
  end

  it 'should be move on the table' do
    @robot.place(1, 1, :north)

    expect(@robot.move).to eq(true)
    expect(@robot.position[:x]).to eq(1)
    expect(@robot.position[:y]).to eq(2)
    expect(@robot.direction).to eq(:north)

    @robot.place(3, 4, :east)
    @robot.move
    @robot.move
    @robot.rotate_left
    @robot.move

    expect(@robot.position[:x]).to eq(5)
    expect(@robot.position[:y]).to eq(5)
    expect(@robot.direction).to eq(:north)

  end

  it 'should be rotate right' do
    @robot.place(1, 1, :north)
    @robot.rotate_right
    expect(@robot.direction).to eq(:east)
    @robot.rotate_right
    expect(@robot.direction).to eq(:south)
    @robot.rotate_right
    expect(@robot.direction).to eq(:west)
    @robot.rotate_right
    expect(@robot.direction).to eq(:north)
    @robot.rotate_right
    expect(@robot.direction).to eq(:east)
  end

  it 'should be rotate left' do
    @robot.place(1, 2, :north)
    @robot.rotate_left
    expect(@robot.direction).to eq(:west)
    @robot.rotate_left
    expect(@robot.direction).to eq(:south)
    @robot.rotate_left
    expect(@robot.direction).to eq(:east)
    @robot.rotate_left
    expect(@robot.direction).to eq(:north)
    @robot.rotate_left
    expect(@robot.direction).to eq(:west)
  end

  it 'should not exit from table' do
    @robot.place(2, 3, :north)
    expect(@robot.move).to eq(true)
    expect(@robot.move).to eq(true)
  end

  it 'should report its position' do
    @robot.place(1, 2, :east)
    expect(@robot.report).to eq("1,2,EAST")
    @robot.move
    expect(@robot.report).to eq("2,2,EAST")
    @robot.rotate_right
    @robot.move
    expect(@robot.report).to eq("2,1,SOUTH")
  end

  it 'should return commands' do
    @robot.command("PLACE 0,0,NORTH")
    expect(@robot.report).to eq("0,0,NORTH")

    @robot.command("MOVE")
    @robot.command("RIGHT")
    @robot.command("MOVE")

    expect(@robot.report).to eq("1,1,EAST")

    for i in 0..10
      @robot.command("MOVE");
    end
    expect(@robot.report).to eq("5,1,EAST")

    for i in 0..3
      @robot.command("LEFT");
    end
    expect(@robot.report).to eq("5,1,EAST")
  end

end