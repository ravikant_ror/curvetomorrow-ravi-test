require_relative 'lib/table'
require_relative 'lib/robot'
# require_relative 'lib/game'

class ToyRobotSimulator

  def initialize
    @table = Table.new 4, 4
    @robot = Robot.new @table
  end

  def execute(command)
    begin
      puts @robot.command(command)
    rescue Exception => e
      puts e.message
    end
  end

end


trs = ToyRobotSimulator.new
puts "\n" + "=" * 20
puts "Toy Robot Simulator\n"
puts "=" * 20
puts "Enter a command. Valid commands are:"
puts "\tPLACE X,Y,<NORTH|SOUTH|EAST|WEST>"
puts "\tMOVE"
puts "\tLEFT"
puts "\tRIGHT"
puts "\tREPORT"
puts "\tEXIT (which will also end the simulator)"

command = STDIN.gets

while command
  command.strip!
  if command.downcase == "exit"
    puts "\n" + "=" * 20
		puts "Thank you for using Toy Robot Simulator\n"
		puts "=" * 20
    exit
  else
    output = trs.execute(command)
    puts output if output
    command = STDIN.gets
  end
end

