class Table
  attr_reader :rows, :columns
  attr_accessor :table

  def initialize(rows, columns)
    raise TypeError, 'Invalid rows or columns value' unless rows.is_a? Integer and columns.is_a? Integer

    @rows = rows
    @columns = columns

    @table = Array.new(5) { Array.new(5, "_") }
    @table_size = 5 - 1
  end

  def piece_removed(pos)
    @table[(pos[:x] - @table_size).abs][pos[:y]] = "_"
    return @table
  end
  
  def piece_added(pos)
    @table[(pos[:x] -  @table_size).abs][pos[:y]] = "X"
    return @table
  end
  
  def print_table
    puts @table.map { |x| x.join(" ") }
    puts "\n"
    return @table
  end

end